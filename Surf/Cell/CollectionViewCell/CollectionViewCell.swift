import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet var textLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        contentView.layer.cornerRadius = 12
        contentView.backgroundColor = greyCellColor
    }

    func render(text: String, isSelected: Bool) {

        textLabel.text = text
        if isSelected {
            contentView.backgroundColor = .black
            textLabel.textColor = .white
        } else {
            contentView.backgroundColor = greyCellColor
            textLabel.textColor = .black
        }
    }

}
