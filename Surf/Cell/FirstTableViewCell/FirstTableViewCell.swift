import UIKit

class FirstTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet private var mainLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!

    static let identifier = "FirstTableViewCell"
    static func nib() -> UINib { return UINib(nibName: "FirstTableViewCell", bundle: nil) }

    private var collectionData: [CollectionCell] = []

    override func awakeFromNib() {
        super.awakeFromNib()

        setupUi()
    }

    func render(model: SurfModel) {
        mainLabel.text = model.mainLabel
        descriptionLabel.text = model.description
        collectionData = model.collectionData
        collectionView.reloadData()
    }

    private func setupUi() {
        mainLabel.font = UIFont.boldSystemFont(ofSize: 24)
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        descriptionLabel.textColor = greyTextColor

        let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "collectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.minimumLineSpacing = 12
        flowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = flowLayout
    }

    // collection functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count * 6
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell",
                                                      for: indexPath) as! CollectionViewCell
        var index = indexPath.item
        if index > collectionData.count - 1 {
            index -= collectionData.count
        }

        let model = collectionData[index % collectionData.count]
        cell.render(text: model.text, isSelected: false)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        var offset = collectionView.contentOffset
        let width = collectionView.contentSize.width

        if offset.x < width / 4 {
            offset.x += width / 2
            collectionView.setContentOffset(offset, animated: false)
        } else if offset.x > width / 4 * 3 {
            offset.x -= width / 2
            collectionView.setContentOffset(offset, animated: false)
        }
    }
}

