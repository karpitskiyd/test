import UIKit

class SecondTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!

    static let identifier = "SecondTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "SecondTableViewCell", bundle: nil)
    }

    private var collectionData: [CollectionCell] = []

    override func awakeFromNib() {
        super.awakeFromNib()

        setupUi()
    }

    func render(model: SurfModel) {
        descriptionLabel.text = model.description
        collectionData = model.collectionData
    }

    private func setupUi() {
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        descriptionLabel.textColor = greyTextColor

        let nib = UINib(nibName: "CollectionViewCell", bundle:nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "collectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.minimumLineSpacing = 12
        flowLayout.minimumInteritemSpacing = 12
        flowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = flowLayout
    }

    // collection functions
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! CollectionViewCell
        cell.render(text: collectionData[indexPath.row].text,
                    isSelected: collectionData[indexPath.row].selected)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !collectionData.isEmpty {
            let item = collectionData.remove(at: indexPath.row)
            if item.selected {

                let newItem = CollectionCell(text: item.text, selected: false)
                collectionData.insert(newItem, at: indexPath.row)
                collectionView.reloadData()
            } else {

                let newItem = CollectionCell(text: item.text, selected: true)
                collectionData.insert(newItem, at: 0)
                setTransition(collectionView: collectionView)
                collectionView.scrollToItem(
                    at: IndexPath(item: 0, section: 0),
                    at: .centeredHorizontally,
                    animated: true
                )
                collectionView.reloadData()
            }
        }
    }

    private func setTransition(collectionView: UICollectionView) {
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        transition.fillMode = CAMediaTimingFillMode.forwards
        transition.duration = 0.5
        transition.subtype = CATransitionSubtype.fromLeft
        collectionView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
    }
}

