import Foundation
import UIKit

let greyTextColor = UIColor(red: 0.588, green: 0.584, blue: 0.608, alpha: 1)
let greyCellColor = UIColor(red: 0.953, green: 0.953, blue: 0.961, alpha: 1)
