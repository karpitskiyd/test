import Foundation

struct SurfModel {
    let mainLabel: String?
    let description: String
    let collectionData: [CollectionCell]
}

struct CollectionCell {
    let text: String
    let selected: Bool
}
