import Foundation
import UIKit

protocol SurfPresenterDelegate: AnyObject {
    func presentModels(model: [SurfModel])
}

typealias PresenterDelegate = SurfPresenterDelegate & UIViewController

class SurfPresenter {

    weak var delegate: PresenterDelegate?

    public func setViewDelegate(delegate: PresenterDelegate) {
        self.delegate = delegate
    }

    public func getModels() {
        let firstModel = SurfModel(mainLabel: "Main.firstCell.title".localized,
                                   description: "Main.firstCell.description".localized,
                                   collectionData: [CollectionCell(text: "IOS", selected: false),
                                                    CollectionCell(text: "Android", selected: false),
                                                    CollectionCell(text: "Design", selected: false),
                                                    CollectionCell(text: "QA", selected: false),
                                                    CollectionCell(text: "Flutter", selected: false),
                                                    CollectionCell(text: "PM", selected: false)])
        let secondModel = SurfModel(mainLabel: nil,
                                    description: "Main.secondCell.description".localized,
                                   collectionData: [CollectionCell(text: "Flutter", selected: false),
                                                    CollectionCell(text: "Tinkoff", selected: false),
                                                    CollectionCell(text: "Koshelek", selected: false),
                                                    CollectionCell(text: "Android", selected: false),
                                                    CollectionCell(text: "IOS", selected: false),
                                                    CollectionCell(text: "Design", selected: false),
                                                    CollectionCell(text: "QA", selected: false),
                                                    CollectionCell(text: "PM", selected: false),
                                                    CollectionCell(text: "Surf", selected: false),
                                                    CollectionCell(text: "CodePilots", selected: false)])
        delegate?.presentModels(model: [firstModel, secondModel])
    }

    public func getAllert() {
        let message = "Main.alert.text".localized
        let title = "Main.alert.title".localized
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Main.alert.button".localized, style: .cancel, handler: nil))
        delegate?.present(alert, animated: true)
    }
}

