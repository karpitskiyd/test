import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, SurfPresenterDelegate {

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var backView: UIView!
    @IBOutlet private var sendButton: UIButton!
    @IBOutlet private var questionLabel: UILabel!
    @IBOutlet private var topViewLayoutConstraint: NSLayoutConstraint!

    private let presenter = SurfPresenter()
    private var cellsModels = [SurfModel]()
    private let downBackViewInset = UIScreen.main.bounds.height * 0.35

    @IBAction private func sendButtonTapped(_ sender: UIButton!) {
        presenter.getAllert()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        setupUi()
        presenter.setViewDelegate(delegate: self)
        presenter.getModels()
    }

    func presentModels(model: [SurfModel]) {
        self.cellsModels = model
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    private func setupUi() {
        sendButton.layer.cornerRadius = 26
        sendButton.titleLabel?.font =  .boldSystemFont(ofSize: 16)
        sendButton.titleLabel?.text = "Main.staticBar.button".localized
        sendButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.58).isActive = true
        backView.layer.cornerRadius = 24
        backView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        questionLabel.font = UIFont.systemFont(ofSize: 12)
        questionLabel.textColor = greyTextColor
        questionLabel.text = "Main.staticBar.label".localized

        tableView.register(FirstTableViewCell.nib(),
                           forCellReuseIdentifier: FirstTableViewCell.identifier)
        tableView.register(SecondTableViewCell.nib(),
                           forCellReuseIdentifier: SecondTableViewCell.identifier)
        tableView.separatorColor = .white
        tableView.contentInset.top = downBackViewInset
        topViewLayoutConstraint.constant = -(tableView.contentOffset.y)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: FirstTableViewCell.identifier,
                                                     for: indexPath) as! FirstTableViewCell
            cell.render(model: cellsModels[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SecondTableViewCell.identifier,
                                                     for: indexPath) as! SecondTableViewCell
            cell.render(model: cellsModels[indexPath.row])
            return cell
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let update = -(tableView.contentOffset.y)
        topViewLayoutConstraint.constant = update
    }

    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        let update = -(tableView.contentOffset.y)
        setViewHeight(height: update)
    }

    func setViewHeight(height: Double) {
        let translation = tableView.panGestureRecognizer.translation(in: tableView.superview)
        if height < downBackViewInset && translation.y < 0 {
            animateTopHeight()
        }

        if height > view.safeAreaInsets.top && translation.y > 0 {
            animateLowHeight()
        }
    }

    private func animateTopHeight() {
        UIView.animate(withDuration: 0.25, animations: { [self] in
            let topSafeArea = view.safeAreaInsets.top
            tableView.contentInset.top = topSafeArea
            view.layoutIfNeeded()
        })
    }

    private func animateLowHeight() {
        UIView.animate(withDuration: 0.25, animations: { [self] in
            tableView.contentInset.top = downBackViewInset
            view.layoutIfNeeded()
        })
    }
}

